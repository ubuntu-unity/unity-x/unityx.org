+++
title = "UnityX 10.0-rc2" 
subtitle = ""
date = "2021-08-02"
draft = false
+++

* Includes a new sidebar design for the application launcher and the opened apps view.
* Added system notifications.
* Added tray icons.
* The separate WiFi, Bluetooth and PulseAudio programs have been replaced with tray icons.
* The application launcher and 'opened apps' shortcuts have been moved into the panel.
* Added logout button.
* Added time next to date.

## Release
**Download link**: https://unityx.org/deb.zip

**GitLab release**: https://gitlab.com/unity-x/unityx/-/releases/10.0-rc2

## Screenshots

<br>

<img src="https://discourse.ubuntu.com/uploads/short-url/yWCxERHcv08zWAJ69k2HwgBQvW1.jpeg" width="100%" height="100%" title="New app launcher" alt="screenshot-1">
<br><br>
<img src="https://discourse.ubuntu.com/uploads/short-url/laDdGbLicKVBeItDQ0P60P28RgF.jpeg" width="100%" height="100%" title="Notifications" alt="screenshot-2">
<br><br>
<img src="https://discourse.ubuntu.com/uploads/short-url/6jTgp7w2pNooE9Y85hbIZkgcdXS.jpeg" width="100%" height="100%" title="Tray icons" alt="screenshot-3">
