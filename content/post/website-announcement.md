+++
title = "Announcing the UnityX website"
subtitle = "Continuing the Ubuntu Discourse thread."
date = "2021-02-12"
draft = false
+++

The UnityX website has finally been set up. This will help to provide more information to people who are not familiar with *developer and maintainer's* talk on **Ubuntu Discourse**.
