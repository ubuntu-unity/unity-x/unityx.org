+++
title = "pages"
+++

# Welcome to the UnityX website!

<img src="/images/unityx-screenshot.png" width="100%" height="100%" title="New app launcher" alt="screenshot-1">

UnityX is the successor of Unity7.

## Links

1. UnityX development thread (the new blog posts are posted here, and the ones below are really outdated): https://discourse.ubuntu.com/t/restarting-unity-development-in-2021-progress-report/20217/53
2. UnityX source (free/libre software): https://gitlab.com/unity-x/unityx.git
3. APT repository: https://repo.unityx.org/README.txt
4. `qckdeb` source (free/libre software): https://docs.unityx.org/tools/qckdeb
5. Ubuntu Unity (on Twitter): https://twitter.com/ubuntu_unity
