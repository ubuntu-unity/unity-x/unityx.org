+++
title = "Core"
draft = false
weight = 0 
+++

UnityX uses GTK as the UI toolkit. However, unlike many other desktop environments (including the previous version of Unity, Unity7), it does not employ GNOME components.

## License

Most of the components of UnityX are licensed under the GNU General Public License version 3.

## Documentation

Visit https://docs.unityx.org.
