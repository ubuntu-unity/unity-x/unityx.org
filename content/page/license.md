+++
title = "License" 
draft = false
weight = 0 
+++

Ubuntu Unity, Unity7 and UnityX are mostly licensed under the GNU GPL v3.0 License.

All the licenses can be found in the project repositories at the [Ubuntu Unity](https://gitlab.com/ubuntu-unity), [Unity7](https://gitlab.com/unity-7) and [UnityX](https://gitlab.com/unity-x).
